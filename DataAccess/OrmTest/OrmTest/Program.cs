﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Insight.Database;
using MicroLite;
using MicroLite.Builder;
using MicroLite.Configuration;

namespace OrmTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //MicroLite();
                MicroLiteSqlBuilder();
                MicroLiteJoin();
                //Insight();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        private static void MicroLiteSqlBuilder()
        {
            var sessionFactory = Configure
                .Fluently()
                .ForMsSql2005Connection("Inntopia2")
                .CreateSessionFactory();
            using (IReadOnlySession session = sessionFactory.OpenReadOnlySession())
            {
                var queryBuilder = SqlBuilder
                    .Select("*")
                    .From("Product")
                    .Where("ProductId").IsEqualTo(8);
                    //.ToSqlQuery();

                //queryBuilder = queryBuilder.AndWhere("SupplierId").IsEqualTo(6231031);
                queryBuilder = queryBuilder.AndWhere("SupplierId").In(6231031, 6286600);
                var query = queryBuilder.ToSqlQuery();
                var results = session.Fetch<dynamic>(query);
                foreach (var item in results)
                {
                    // The property names of each dynamic result will match
                    // (including case) the column names specified in the query.
                    Console.WriteLine("{0}-{1}:{2}", item.SupplierId, item.ProductId, item.ProductName);
                }
            }
        }

        private static void Insight()
        {
            ////SqlConnection Database = new SqlConnection();
            //using (SqlConnection connection = Database.Open())
            //using (SqlTransaction t = connection.BeginTransaction())
            //{
            //   // IList<Beer> beer = connection.Dynamic<Beer>().FindBeer(name: "IPA", transaction: t);
            //}
        }

        public static void MicroLite()
        {
            var sessionFactory = Configure
                .Fluently()
                .ForMsSql2005Connection("Inntopia2")
                .CreateSessionFactory();
            
            // Create an ad-hoc query, this could select a number of columns across multiple tables if desired.
            var query = new SqlQuery("SELECT top 22 ProductName, ProductId FROM Product");

            var session = sessionFactory.OpenReadOnlySession();
            // Execute the query and return the results.
            var results = session.Fetch<dynamic>(query);

            foreach (var item in results)
            {
                // The property names of each dynamic result will match
                // (including case) the column names specified in the query.
                Console.WriteLine("{0}:{1}", item.ProductId, item.ProductName);
            }
        }
        public static void MicroLiteJoin()
        {
            var sessionFactory = Configure
                .Fluently()
                .ForMsSql2005Connection("Inntopia2")
                .CreateSessionFactory();

            // Create an ad-hoc query, this could select a number of columns across multiple tables if desired.
            var query = new SqlQuery("SELECT top 22 p.ProductName, p.ProductId, pXr.RSysProductId FROM Product p join RSys_Supplier_Product_XR pXr on pXr.SupplierId = p.SupplierId and pXr.ProductId = p.ProductId");

            var session = sessionFactory.OpenReadOnlySession();
            // Execute the query and return the results.
            var results = session.Fetch<dynamic>(query);

            foreach (var item in results)
            {
                // The property names of each dynamic result will match
                // (including case) the column names specified in the query.
                Console.WriteLine("{0}({1}): {2}", item.ProductId, item.RSysProductId, item.ProductName);
            }
        }
    }
}
