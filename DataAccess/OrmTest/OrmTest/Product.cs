﻿namespace OrmTest
{
    public class Product
    {
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public int SupplierId { get; internal set; }
    }
}