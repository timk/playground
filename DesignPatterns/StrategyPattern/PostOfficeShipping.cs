﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyPattern
{
    public class PostOfficeShipping : IShippingCost
    {
        public decimal CalculateShipping(string destination, int weight)
        {
            return new decimal(1.1);
        }

        public void DetectAdHocAuditIssues(Func<Customer, List<SalesOrder>, bool> auditFunction)
        {
            Customer customer = null;
            List<SalesOrder> salesOrders = new List<SalesOrder>();
            bool success = auditFunction(customer, salesOrders);
        }
    }
}
