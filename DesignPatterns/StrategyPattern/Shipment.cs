﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace StrategyPattern
{
    public class Shipment
    {
        public string CustomerAddress { get; private set; }
        public int Weight { get; private set; }

        public decimal CalculateShipping(IShippingCost shippingCalculator)
        {
            Func<Customer, List<SalesOrder>, bool> foo = (c, o) =>
           o.Count > 9 ? true : false;
            //shippingCalculator.DetectAdHocAuditIssues(foo);
            shippingCalculator.DetectAdHocAuditIssues((c, o) =>
           o.Count > 9 ? true : false);
            decimal retVal = shippingCalculator.CalculateShipping(this.CustomerAddress, this.Weight);
            return retVal;
        }
    }
}
