﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting;
using System.Security.Cryptography.X509Certificates;

namespace StrategyPattern
{
    public interface IShippingCost
    {
        decimal CalculateShipping(string destination, int weight);
        void DetectAdHocAuditIssues(Func<Customer, List<SalesOrder>, bool> auditFunction);
    }
}