﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyPattern
{
    public class FedExShipping : IShippingCost
    {
        public decimal CalculateShipping(string destination, int weight)
        {
            return new decimal(2.2);
        }

        public void DetectAdHocAuditIssues(Func<Customer, List<SalesOrder>, bool> auditFunction)
        {
            throw new NotImplementedException();
        }
    }
}
