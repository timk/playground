﻿namespace TemplatePattern
{
    public interface IMessageAuditor
    {
        void Audit(Message message);
    }
}