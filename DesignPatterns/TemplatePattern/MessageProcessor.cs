﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace TemplatePattern
{
    public abstract class MessageProcessor
    {
        private readonly IMessageAuditor _messageAuditor;

        public MessageProcessor(MessageAuditor messageAuditor)
        {
            _messageAuditor = messageAuditor;
        }

        public void ProcessMessage(Message message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));

            Authorize(message);
            DoWork(message);
        }

        protected abstract void Authorize(Message message);
        protected abstract void DoWork(Message message);
        protected abstract void Audit(Message message, AuditType auditType);
    }
    
}
