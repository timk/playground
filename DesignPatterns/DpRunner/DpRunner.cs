﻿using StrategyPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplatePattern;

namespace DesignPatterns
{
    class DpRunner
    {
        static void Main(string[] args)
        {
            try
            {
                RunStrategyPattern();

                MessageProcessor messageProcessor = null;//new MessageProcessor();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        private static void RunStrategyPattern()
        {
            Shipment shipment = new Shipment();
            IShippingCost shippingCalculator = new PostOfficeShipping();
            decimal shipping = shipment.CalculateShipping(shippingCalculator);

        }
    }
}
