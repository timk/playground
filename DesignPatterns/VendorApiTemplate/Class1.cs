﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using TemplatePattern;

namespace VendorApiTemplate
{
    public enum AuditContext { Begin = 0, End, Info, Error }
    public abstract class ServiceTemplate
    {
        public IMessageAuditor Auditor { get; set; }
        protected void ProcessMessage(Message message)
        {
            try
            {
                Authenticate(message);
                Audit(message, AuditContext.Begin);
                DoWork(message);
                Audit(message, AuditContext.End);
            }
            catch (Exception ex)
            {
                Log(ex);
                throw;
            }
        }

        internal abstract void Audit(Message message, AuditContext begin);
        protected abstract void Authenticate(Message message);

        protected void Audit(Message message)
        {
            this.Auditor.Audit(message);
        }
        protected abstract void DoWork(Message message);

        private void Log(Exception ex)
        {
            throw new NotImplementedException();
        }
    }
}
